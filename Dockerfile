FROM clux/muslrust:1.65.0 AS base

RUN cargo install cargo-chef --locked

FROM base AS planner
COPY . .
RUN cargo chef prepare --bin s4g_backend --recipe-path recipe.json

FROM base AS builder
COPY --from=planner /volume/recipe.json recipe.json
RUN cargo chef cook --bin s4g_backend --release --target x86_64-unknown-linux-musl --recipe-path recipe.json
COPY . .
RUN cargo build --bin s4g_backend --release --target x86_64-unknown-linux-musl
RUN strip -s target/x86_64-unknown-linux-musl/release/s4g_backend

FROM gcr.io/distroless/static:nonroot AS runtime
COPY --from=builder --chown=nonroot:nonroot /volume/target/x86_64-unknown-linux-musl/release/s4g_backend /app/run
CMD ["/app/run"]
