CREATE TABLE providers (
    id INTEGER PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    name VARCHAR UNIQUE NOT NULL,
    url_conversions INTEGER NOT NULL CHECK (url_conversions >= 0),
    enabled BOOLEAN NOT NULL
);

CREATE INDEX providers_id_index ON providers(id);
CREATE INDEX providers_name_index ON providers(name);

INSERT INTO providers(name, url_conversions, enabled)
VALUES('bitly', 0, TRUE);
INSERT INTO providers(name, url_conversions, enabled)
VALUES('tinyurl', 0, TRUE);
