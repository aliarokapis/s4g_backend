FROM clux/muslrust:stable AS builder
RUN cargo install --target x86_64-unknown-linux-musl

FROM scratch 
COPY from=builder /root/.cargo/bin/diesel /diesel
CMD ["/diesel"]
