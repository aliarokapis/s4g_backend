use anyhow::Context;
use s4g_backend::config::Config;
use s4g_backend::startup::Application;
use s4g_backend::telemetry;

#[actix_web::main]
async fn main() -> anyhow::Result<()> {
    dotenvy::dotenv().ok();

    openssl_probe::init_ssl_cert_env_vars();

    let config = Config::from_env()
        .build()
        .context("Failed to create configuration.")?;

    let subscriber = telemetry::get_subscriber("s4g".into(), "info".into(), std::io::stdout);

    telemetry::install_telemetry(subscriber).context("Failed to install subscriber.")?;

    let app = Application::build(&config).await?;

    app.run().await?;

    telemetry::terminate_telemetry()
        .await
        .context("Failed to terminate telemetry.")?;

    Ok(())
}
