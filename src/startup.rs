use std::net::TcpListener;

use actix_cors::Cors;
use actix_web::dev::Server;
use actix_web::{web, App, HttpServer};
use actix_web_opentelemetry::{RequestMetricsBuilder, RequestTracing};
use anyhow::Context;
use deadpool_diesel::postgres::{Manager, Pool, Runtime};
use opentelemetry::global;
use reqwest_middleware::ClientBuilder;
use reqwest_tracing::TracingMiddleware;
use tracing_actix_web::TracingLogger;
use utoipa::OpenApi;
use utoipa_swagger_ui::SwaggerUi;

use crate::config;
use crate::domain;
use crate::{bitly, provider, routes, tinyurl};

trait AllowedOrigins: Sized {
    fn allowed_origins<T: AsRef<str>>(self, origins: impl Iterator<Item = T>) -> Self;
}

impl AllowedOrigins for Cors {
    fn allowed_origins<T: AsRef<str>>(mut self, origins: impl Iterator<Item = T>) -> Self {
        for origin in origins {
            self = self.allowed_origin(origin.as_ref());
        }
        self
    }
}

#[derive(OpenApi)]
#[openapi(
    paths(routes::put_shorten, routes::get_health_check),
    components(schemas(
        routes::shorten::ShortenRequest,
        routes::shorten::ShortenResponse,
        provider::ProviderOpt,
        domain::Url
    ))
)]
struct UserApiDoc;

#[derive(OpenApi)]
#[openapi(
    paths(
        routes::provider::get_index,
        routes::provider::get_show,
        routes::provider::put_status,
    ),
    components(schemas(
        routes::provider::show::ProviderInfo,
        routes::provider::show::ProviderInfo,
        routes::provider::status::ProviderSetStatusRequest,
        provider::ProviderOpt,
    ))
)]
struct AdminApiDoc;

pub struct Application {
    port: u16,
    server: Server,
}

impl Application {
    pub async fn build(config: &config::Config) -> anyhow::Result<Self> {
        let address = format!("{}:{}", config.app.host, config.app.port);
        let listener = TcpListener::bind(address).context("failed to create TcpListener")?;
        let port = listener.local_addr().unwrap().port();
        let server = run(config, listener).await?;
        Ok(Application { port, server })
    }

    pub fn port(&self) -> u16 {
        self.port
    }

    pub async fn run(self) -> std::io::Result<()> {
        self.server.await
    }
}

pub async fn run(config: &config::Config, listener: TcpListener) -> anyhow::Result<Server> {
    let http_client = ClientBuilder::new(reqwest::Client::new())
        .with(TracingMiddleware::default())
        .build();

    let bitly = web::Data::new(bitly::Client {
        http_client: http_client.clone(),
        base_url: config.bitly.base_url.clone(),
        bearer: config.bitly.bearer.clone(),
    });

    let tinyurl = web::Data::new(tinyurl::Client {
        http_client,
        base_url: config.tinyurl.base_url.clone(),
        bearer: config.tinyurl.bearer.clone(),
    });

    let manager = Manager::new(config.database.with_db_name(), Runtime::Tokio1);

    let db_pool = web::Data::new(
        Pool::builder(manager)
            .max_size(config.app.thread_pool_size as usize)
            .build()
            .context("Failed to construct db pool")?,
    );

    let allowed_origins = config.app.allowed_origins.clone();

    let meter = global::meter("");

    let server = HttpServer::new(move || {
        App::new()
            .wrap(TracingLogger::default())
            .wrap(RequestTracing::new())
            .wrap(RequestMetricsBuilder::new().build(meter.clone()))
            .wrap(
                Cors::default()
                    .allow_any_method()
                    .allow_any_header()
                    .allowed_origins(allowed_origins.iter()),
            )
            .app_data(bitly.clone())
            .app_data(tinyurl.clone())
            .app_data(db_pool.clone())
            .service(
                SwaggerUi::new("/swagger-ui/{_:.*}")
                    .url("/api/v1/user-api-doc/openapi.json", UserApiDoc::openapi())
                    .url("/api/v1/admin-api-doc/openapi.json", AdminApiDoc::openapi()),
            )
            .service(routes::get_health_check)
            .service(routes::put_shorten)
            .service(routes::provider::get_index)
            .service(routes::provider::get_show)
            .service(routes::provider::put_status)
    })
    .listen(listener)?
    .run();

    Ok(server)
}
