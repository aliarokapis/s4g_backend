use deadpool_diesel::postgres::Pool;
use diesel::prelude::{Identifiable, Insertable, Queryable};
use serde::{Deserialize, Serialize};
use utoipa::ToSchema;

use crate::schema::providers;
use crate::utils::{query, QueryError};

#[derive(Deserialize, Serialize, Hash, Debug, ToSchema, Clone)]
pub enum ProviderOpt {
    #[serde(rename = "bitly")]
    Bitly,
    #[serde(rename = "tinyurl")]
    Tinyurl,
}

#[derive(Queryable, Identifiable, Insertable, Debug, Clone)]
pub struct Provider {
    pub id: i32,
    pub name: String,
    pub url_conversions: i32,
    pub enabled: bool,
}

impl std::fmt::Display for ProviderOpt {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match &self {
            ProviderOpt::Bitly => "bitly".fmt(f),
            ProviderOpt::Tinyurl => "tinyurl".fmt(f),
        }
    }
}

#[derive(thiserror::Error, Debug, Clone)]
pub enum ProviderOptParseError {
    #[error("Cannot convert {0} to ProviderOpt.")]
    InvalidRepresentation(String),
}

impl std::str::FromStr for ProviderOpt {
    type Err = ProviderOptParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "bitly" => Ok(ProviderOpt::Bitly),
            "tinyurl" => Ok(ProviderOpt::Tinyurl),
            _ => Err(ProviderOptParseError::InvalidRepresentation(s.into())),
        }
    }
}

impl Default for ProviderOpt {
    fn default() -> ProviderOpt {
        ProviderOpt::Bitly
    }
}

#[derive(thiserror::Error, Debug)]
pub enum FindProviderError {
    #[error(transparent)]
    QueryFailed(#[from] QueryError),
    #[error("Provider {0} not found")]
    ProviderNotFound(ProviderOpt),
}

#[tracing::instrument(skip(pool))]
pub async fn find_provider(opt: &ProviderOpt, pool: &Pool) -> Result<Provider, FindProviderError> {
    let opt_name = opt.to_string();

    let copy = opt_name.clone();

    query(pool, move |conn| {
        use diesel::prelude::*;

        use crate::schema::providers::dsl::*;

        providers.filter(name.eq(copy)).first(conn).optional()
    })
    .await?
    .ok_or_else(|| FindProviderError::ProviderNotFound(opt.clone()))
}

#[derive(thiserror::Error, Debug)]
pub enum SetStatusError {
    #[error(transparent)]
    ProviderNotFound(#[from] FindProviderError),
    #[error(transparent)]
    QueryFailed(#[from] QueryError),
}

#[tracing::instrument(skip(pool))]
pub async fn set_status(
    opt: &ProviderOpt,
    pool: &Pool,
    status: bool,
) -> Result<(), SetStatusError> {
    let provider = find_provider(opt, pool).await?;

    query(pool, move |conn| {
        use diesel::prelude::*;

        use crate::schema::providers::dsl::*;

        diesel::update(&provider)
            .set(enabled.eq(status))
            .execute(conn)
    })
    .await?;

    Ok(())
}

#[derive(thiserror::Error, Debug)]
pub enum IncUrlConversionsError {
    #[error(transparent)]
    QueryFailed(#[from] QueryError),
}

#[tracing::instrument(skip(pool))]
pub async fn inc_url_conversions(
    provider: &Provider,
    pool: &Pool,
) -> Result<(), IncUrlConversionsError> {
    let provider = provider.clone();

    query(pool, move |conn| {
        use diesel::prelude::*;

        use crate::schema::providers::dsl::*;

        diesel::update(&provider)
            .set(url_conversions.eq(provider.url_conversions + 1))
            .execute(conn)
    })
    .await?;

    Ok(())
}

#[derive(thiserror::Error, Debug)]
pub enum RetrieveAllProvidersError {
    #[error(transparent)]
    QueryFailed(#[from] QueryError),
}

#[tracing::instrument(skip(pool))]
pub async fn retrieve_all_providers(
    pool: &Pool,
) -> Result<Vec<Provider>, RetrieveAllProvidersError> {
    Ok(query(pool, move |conn| {
        use diesel::prelude::*;

        use crate::schema::providers::dsl::*;

        providers.order_by(name).load(conn)
    })
    .await?)
}
