use anyhow::Context;
use secrecy::{ExposeSecret, Secret};
use serde::{Deserialize, Serialize};

use crate::domain::{Host, Url};

//use serde_json::Value;

//#[derive(Serialize, Deserialize, Debug)]
//pub struct DeepLink {
//    pub references: Vec<Value>,
//    pub bitlink: Url,
//    pub install_url: String,
//    pub created: String,
//    pub app_uri_path: String,
//    pub modified: String,
//    pub install_type: String,
//    pub app_guid: String,
//    pub guid: String,
//    pub os: String,
//    pub brand_guid: String,
//}

#[derive(Serialize, Deserialize, Debug)]
pub struct ShortenBitlinkBody {
    //pub archived: bool,
    //pub tags: Vec<String>,
    //pub created_at: String,
    //pub deeplinks: Vec<DeepLink>,
    //pub long_url: String,
    //pub custom_bitlinks: Vec<String>,
    pub link: Url,
    //pub id: String,
}

#[derive(Serialize, Debug)]
pub struct ShortenUrlRequest {
    pub long_url: Url,
    pub group_guid: Option<String>,
    pub domain: Option<Host>,
}

pub type ShortenUrlResponse = ShortenBitlinkBody;

#[derive(Debug, Clone)]
pub struct Client {
    pub http_client: reqwest_middleware::ClientWithMiddleware,
    pub base_url: String,
    pub bearer: Secret<String>,
}

impl Client {
    #[tracing::instrument(skip(self, request))]
    pub async fn shorten_url(
        &self,
        request: &ShortenUrlRequest,
    ) -> anyhow::Result<ShortenUrlResponse> {
        let url = format!("{}/v4/shorten", self.base_url);

        let response = self
            .http_client
            .post(url)
            .bearer_auth(&self.bearer.expose_secret())
            .json(&request)
            .send()
            .await
            .context("Reqwest failed to get response from bitly.")?
            .error_for_status()
            .context("Unsuccessful response status.")?;

        response
            .json::<ShortenBitlinkBody>()
            .await
            .context("Failed to deserialize response.")
    }
}
