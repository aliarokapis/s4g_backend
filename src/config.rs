use config::{builder::DefaultState, ConfigBuilder, ConfigError};
use secrecy::{ExposeSecret, Secret};
use serde::Deserialize;

#[derive(Deserialize, Clone, Debug)]
pub struct DatabaseConfig {
    pub host: String,
    pub username: String,
    pub password: Secret<String>,
    pub db_name: String,
}

impl DatabaseConfig {
    pub fn without_db_name(&self) -> String {
        format!(
            "postgres://{}:{}@{}",
            self.username,
            self.password.expose_secret(),
            self.host
        )
    }

    pub fn with_db_name(&self) -> String {
        format!("{}/{}", self.without_db_name(), self.db_name)
    }
}

#[derive(Deserialize, Clone, Debug)]
pub struct AppConfig {
    pub host: String,
    pub port: u16,
    pub allowed_origins: Vec<String>,
    pub thread_pool_size: u32,
}

#[derive(Deserialize, Clone, Debug)]
pub struct BitlyConfig {
    pub base_url: String,
    pub bearer: Secret<String>,
}

#[derive(Deserialize, Clone, Debug)]
pub struct TinyUrlConfig {
    pub base_url: String,
    pub bearer: Secret<String>,
}

#[derive(Deserialize, Clone, Debug)]
pub struct Config {
    pub app: AppConfig,
    pub bitly: BitlyConfig,
    pub tinyurl: TinyUrlConfig,
    pub database: DatabaseConfig,
}

#[derive(thiserror::Error, Debug)]
pub enum Error {
    #[error("Failed to build config")]
    BuildError(#[source] ConfigError),

    #[error(transparent)]
    UnexpectedError(ConfigError),
}

pub struct Builder(Result<ConfigBuilder<DefaultState>, ConfigError>);

impl Config {
    pub fn from_env() -> Builder {
        Builder(Ok(config::Config::builder().add_source(
            config::Environment::with_prefix("S4G")
                .prefix_separator("__")
                .separator("__")
                .list_separator(",")
                .with_list_parse_key("app.allowed_origins")
                .try_parsing(true),
        )))
    }
}

impl Builder {
    pub fn set_app_host(self, host: String) -> Self {
        Builder((move || self.0?.set_override("app.host", host))())
    }

    pub fn set_app_port(self, port: u16) -> Self {
        Builder((move || self.0?.set_override("app.port", port))())
    }

    pub fn set_app_thread_pool_size(self, size: u32) -> Self {
        Builder((move || self.0?.set_override("app.thread_pool_size", size))())
    }

    pub fn set_app_allowed_origins(self, origins: Vec<String>) -> Self {
        Builder((move || self.0?.set_override("app.allowed_origins", origins))())
    }

    pub fn set_database_db_name(self, name: String) -> Self {
        Builder((move || self.0?.set_override("database.db_name", name))())
    }

    pub fn set_bitly_base_url(self, url: String) -> Self {
        Builder((move || self.0?.set_override("bitly.base_url", url))())
    }

    pub fn set_bitly_bearer(self, bearer: String) -> Self {
        Builder((move || self.0?.set_override("bitly.bearer", bearer))())
    }

    pub fn set_tinyurl_base_url(self, url: String) -> Self {
        Builder((move || self.0?.set_override("tinyurl.base_url", url))())
    }

    pub fn set_tinyurl_bearer(self, bearer: String) -> Self {
        Builder((move || self.0?.set_override("tinyurl.bearer", bearer))())
    }

    pub fn build(self) -> Result<Config, Error> {
        self.0
            .map_err(Error::UnexpectedError)?
            .build()
            .map_err(Error::BuildError)?
            .try_deserialize::<Config>()
            .map_err(Error::UnexpectedError)
    }
}
