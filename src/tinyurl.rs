use anyhow::Context;
//use iso8601_timestamp::Timestamp;
use secrecy::{ExposeSecret, Secret};
use serde::{Deserialize, Serialize};
use serde_repr::{Deserialize_repr, Serialize_repr};
use valuable::Valuable;

use crate::domain::Url;

#[derive(Serialize, Debug)]
pub struct CreateRequest {
    pub url: Url,
    //pub domain: Option<String>,
    //pub alias: Option<String>,
    pub tags: Vec<String>,
    //pub expires_at: Option<Timestamp>,
}

//#[derive(Serialize, Deserialize, Debug)]
//pub struct AnalyticsSettings {
//    pub enabled: bool,
//    pub public: bool,
//}
//
//#[test]
//fn analytics_settings_deserializable() {
//    let json = serde_json::json!(
//    {
//        "enabled": true,
//        "public": false
//    });
//
//    assert!(serde_json::from_value::<AnalyticsSettings>(json).is_ok())
//}

#[derive(Serialize, Deserialize, Debug)]
pub struct CreateResponseData {
    //pub domain: String,
    //pub alias: String,
    //pub deleted: bool,
    //pub archived: bool,
    //pub analytics: AnalyticsSettings,
    //pub tags: Vec<String>,
    //pub created_at: Option<Timestamp>,
    //pub expires_at: Option<Timestamp>,
    pub tiny_url: Url,
    //pub url: Url,
}

#[test]
fn create_response_data_deserializable() {
    let json = serde_json::json!(
    {
        "domain": "tinyurl.com",
        "alias": "3ukuuaz2",
        "deleted": false,
        "archived": false,
        "analytics": {
            "enabled": true,
            "public": false
        },
        "tags": [ ],
        "created_at": "2022-10-25T21:55:21+00:00",
        "expires_at": null,
        "tiny_url": "https://tinyurl.com/3ukuuaz2",
        "url": "http://www.test.com/"
    });

    assert!(serde_json::from_value::<CreateResponseData>(json).is_ok())
}

#[derive(Serialize_repr, Deserialize_repr, Debug)]
#[repr(u8)]
pub enum SuccessCode {
    Ok = 0,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct SuccessCreateResponse {
    pub code: SuccessCode,
    pub data: CreateResponseData,
}

#[test]
fn success_create_response_deserializable() {
    let json = serde_json::json!(
    {
      "data": {
          "domain": "tinyurl.com",
          "alias": "3ukuuaz2",
          "deleted": false,
          "archived": false,
          "analytics": {
              "enabled": true,
              "public": false
          },
          "tags": [ ],
          "created_at": "2022-10-25T21:55:21+00:00",
          "expires_at": null,
          "tiny_url": "https://tinyurl.com/3ukuuaz2",
          "url": "http://www.test.com/"
      },
      "code": 0,
      "errors": [ ]
    });

    assert!(serde_json::from_value::<SuccessCreateResponse>(json).is_ok());
}

#[derive(Serialize_repr, Deserialize_repr, Debug, Eq, PartialEq, Valuable)]
#[repr(u8)]
pub enum ErrorCode {
    NotAuthorized = 1,
    NotImplemented = 2,
    Forbidden = 3,
    PermissionDenied = 4,
    ValidationFails = 5,
    NotFound = 6,
    InternalServerError = 7,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct ErrorResponse {
    pub code: ErrorCode,
    pub errors: Vec<String>,
}

#[test]
fn error_response_not_authorized_deserializable() {
    let json = serde_json::json!(
    {
      "data": [],
      "code": 1,
      "errors": [ ]
    });

    let parsed = serde_json::from_value::<ErrorResponse>(json).unwrap();
    assert_eq!(parsed.code, ErrorCode::NotAuthorized);
}

#[test]
fn error_response_forbidden_deserializable() {
    let json = serde_json::json!(
    {
      "data": [],
      "code": 3,
      "errors": [ ]
    });

    let parsed = serde_json::from_value::<ErrorResponse>(json).unwrap();
    assert_eq!(parsed.code, ErrorCode::Forbidden);
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(untagged)]
pub enum CreateResponse {
    Success(Box<SuccessCreateResponse>),
    Error(ErrorResponse),
}

#[derive(Debug, Clone)]
pub struct Client {
    pub http_client: reqwest_middleware::ClientWithMiddleware,
    pub base_url: String,
    pub bearer: Secret<String>,
}

impl Client {
    #[tracing::instrument(skip(self, request))]
    pub async fn create(&self, request: &CreateRequest) -> anyhow::Result<CreateResponse> {
        let url = format!("{}/create", self.base_url);

        let response = self
            .http_client
            .post(url)
            .bearer_auth(&self.bearer.expose_secret())
            .json(&request)
            .send()
            .await
            .context("Reqwest failed to get response from tinyurl.")?;

        let text = response
            .text()
            .await
            .context("Failed to retrieve text from tinyurl response.")?;

        serde_json::de::from_str::<CreateResponse>(&text).context("Failed to deserialize response.")
    }
}
