use actix_json_responder::JsonResponder;
use actix_web::{get, web, ResponseError};
use anyhow::Context;
use deadpool_diesel::postgres::Pool;
use serde::{Deserialize, Serialize};
use utoipa::{IntoParams, ToSchema};

use crate::provider::{find_provider, ProviderOpt};

#[derive(Serialize, JsonResponder, Debug, ToSchema)]
pub struct ProviderInfo {
    #[schema(example = "bitly")]
    pub provider: ProviderOpt,

    #[schema(example = 1234)]
    pub url_conversions: i32,

    pub enabled: bool,
}

#[derive(Serialize, JsonResponder, Debug)]
pub struct ProviderShowResponse(ProviderInfo);

#[derive(thiserror::Error, Debug)]
pub enum ProviderShowError {
    #[error("Encountered unexpected error.")]
    UnexpectedError(#[from] anyhow::Error),
}

impl ResponseError for ProviderShowError {}

#[derive(Deserialize, Debug, IntoParams, Clone)]
#[into_params(names("name"))]
struct NameParam(ProviderOpt);

#[utoipa::path(
    responses(
        (status=200, description="Retrieve Provider information.", body=ProviderInfo),
        (status=400,
            description="Malformed path parameter.",
            body=String,
            example = json!("Unknown variant X, expected `bitly` or `tinyurl`."))
    ),
    params(NameParam)
)]
#[get("/api/v1/provider/{name}")]
#[tracing::instrument(skip(path, pool))]
pub async fn show(
    path: web::Path<ProviderOpt>,
    pool: web::Data<Pool>,
) -> Result<ProviderShowResponse, ProviderShowError> {
    let provider = find_provider(&path, &pool)
        .await
        .context("Find provider failed")?;

    Ok(ProviderShowResponse(ProviderInfo {
        provider: std::str::FromStr::from_str(&provider.name)
            .context("Read incompatible provider from database")?,
        url_conversions: provider.url_conversions,
        enabled: provider.enabled,
    }))
}
