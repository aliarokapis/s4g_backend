use actix_web::{put, web, HttpResponse, ResponseError};
use anyhow::Context;
use deadpool_diesel::postgres::Pool;
use serde::Deserialize;
use utoipa::{IntoParams, ToSchema};

use crate::provider::{set_status, ProviderOpt};

#[derive(Deserialize, Debug, ToSchema)]
pub struct ProviderSetStatusRequest {
    pub enable: bool,
}

#[derive(thiserror::Error, Debug)]
pub enum ProviderStatusError {
    #[error("Encountered unexpected error.")]
    UnexpectedError(#[from] anyhow::Error),
}

impl ResponseError for ProviderStatusError {}

#[derive(Deserialize, Debug, IntoParams, Clone)]
#[into_params(names("name"))]
struct NameParam(ProviderOpt);

#[utoipa::path(
    request_body = ProviderSetStatusRequest,
    responses(
        (status=200, description="Successfully changed Provider status."),
        (status=400,
            description="Malformed path parameter or request body.",
            body=String,
            example = json!("Missing field `enable` at line X column Y."))
    ),
    params(NameParam)
)]
#[put("/api/v1/provider/{name}/status")]
#[tracing::instrument(skip(req, pool))]
pub async fn status(
    req: web::Json<ProviderSetStatusRequest>,
    name: web::Path<ProviderOpt>,
    pool: web::Data<Pool>,
) -> Result<HttpResponse, ProviderStatusError> {
    set_status(&name, &pool, req.enable)
        .await
        .context("Set status failed")?;

    Ok(HttpResponse::Ok().finish())
}
