use actix_json_responder::JsonResponder;
use actix_web::{get, web, ResponseError};
use anyhow::Context;
use deadpool_diesel::postgres::Pool;
use itertools::{Either, Itertools};
use serde::Serialize;
use utoipa::ToSchema;

use super::show::ProviderInfo;
use crate::provider::{retrieve_all_providers, ProviderOptParseError};

#[derive(Serialize, JsonResponder, Debug, ToSchema)]
pub struct ProviderIndexResponse(Vec<ProviderInfo>);

#[derive(thiserror::Error, Debug)]
pub enum ProviderIndexError {
    #[error("Encountered unexpected error.")]
    UnexpectedProviderNamesError(Vec<ProviderOptParseError>),
    #[error("Encountered unexpected error.")]
    UnexpectedError(#[from] anyhow::Error),
}

impl ResponseError for ProviderIndexError {}

#[utoipa::path(
    responses(
        (status=200, description="List supported Providers and relevant statistics.", body=[ProviderInfo])
    )
)]
#[get("/api/v1/provider")]
#[tracing::instrument(skip(pool))]
pub async fn index(pool: web::Data<Pool>) -> Result<ProviderIndexResponse, ProviderIndexError> {
    let (providers, errors): (Vec<ProviderInfo>, Vec<ProviderOptParseError>) =
        retrieve_all_providers(&pool)
            .await
            .context("Retrieving the providers failed.")?
            .into_iter()
            .partition_map(|p| match std::str::FromStr::from_str(&p.name) {
                Ok(n) => Either::Left(ProviderInfo {
                    provider: n,
                    url_conversions: p.url_conversions,
                    enabled: p.enabled,
                }),
                Err(e) => Either::Right(e),
            });

    if !errors.is_empty() {
        return Err(ProviderIndexError::UnexpectedProviderNamesError(errors));
    }

    Ok(ProviderIndexResponse(providers))
}
