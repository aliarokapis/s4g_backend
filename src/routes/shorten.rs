use actix_json_responder::JsonResponder;
use actix_web::{http, put, web, ResponseError};
use anyhow::Context;
use deadpool_diesel::postgres::Pool;
use serde::{Deserialize, Serialize};
use utoipa::ToSchema;

use crate::domain::Url;
use crate::provider::{find_provider, inc_url_conversions, ProviderOpt};

#[derive(Deserialize, Debug, ToSchema)]
pub struct ShortenRequest {
    #[schema(example = "https://wrapp.ai")]
    pub url: Url,

    #[schema(example = "bitly")]
    pub provider: Option<ProviderOpt>,
}

#[derive(Serialize, JsonResponder, Debug, ToSchema)]
#[schema(example = json!({"short_url" : "testest"}))]
pub struct ShortenResponse {
    short_url: Url,
}

#[derive(thiserror::Error, Debug, ToSchema)]
pub enum ShortenError {
    #[error("Provider {0} is currently disabled.")]
    ProviderIsNotEnabled(ProviderOpt),

    #[error("Encountered unexpected error.")]
    ReceivedErrorsFromProvider(Vec<anyhow::Error>),

    #[error("Encountered unexpected error.")]
    UnexpectedError(#[from] anyhow::Error),
}

impl ResponseError for ShortenError {
    fn status_code(&self) -> http::StatusCode {
        match &self {
            ShortenError::ProviderIsNotEnabled(_) => http::StatusCode::CONFLICT,
            _ => http::StatusCode::INTERNAL_SERVER_ERROR,
        }
    }
}

#[tracing::instrument(skip(bitly))]
pub async fn shorten_bitly(
    url: &Url,
    bitly: &crate::bitly::Client,
) -> Result<ShortenResponse, ShortenError> {
    let response = bitly
        .shorten_url(&crate::bitly::ShortenUrlRequest {
            long_url: url.clone(),
            group_guid: None,
            domain: None,
        })
        .await
        .context("Bitly request failed.")?;

    Ok(ShortenResponse {
        short_url: response.link,
    })
}

#[tracing::instrument(skip(tinyurl), fields(error.code, error.details))]
pub async fn shorten_tinyurl(
    url: &Url,
    tinyurl: &crate::tinyurl::Client,
) -> Result<ShortenResponse, ShortenError> {
    let response = tinyurl
        .create(&crate::tinyurl::CreateRequest {
            url: url.clone(),
            tags: Vec::new(),
        })
        .await
        .context("Tinyurl request failed.")?;

    match response {
        crate::tinyurl::CreateResponse::Success(response) => Ok(ShortenResponse {
            short_url: response.data.tiny_url,
        }),
        crate::tinyurl::CreateResponse::Error(err) => {
            tracing::Span::current().record("error.code", tracing::field::valuable(&err.code));
            tracing::Span::current().record("error.details", tracing::field::valuable(&err.errors));

            Err(anyhow::anyhow!(
                "Tinyurl failed with code: {}, errors: {:?}",
                err.code as u8,
                err.errors
            )
            .into())
        }
    }
}

#[utoipa::path(
    request_body=ShortenRequest,
    responses(
        (status=200,
            description="Shorten url using the specified Provider",
            body = ShortenResponse),
        (status=400,
            description="Malformed request body.",
            body=String,
            example = json!("Unknown variant X, expected `bitly` or `tinyurl` at line Y, row Z.")),
        (status=409,
            description="Specified Provider is disabled.",
            body=String,
            example = json!("Provider bitly is currently disabled."))
    )
)]
#[put("/api/v1/shorten")]
#[tracing::instrument(skip(req, bitly, tinyurl, pool))]
pub async fn shorten(
    req: web::Json<ShortenRequest>,
    bitly: web::Data<crate::bitly::Client>,
    tinyurl: web::Data<crate::tinyurl::Client>,
    pool: web::Data<Pool>,
) -> Result<ShortenResponse, ShortenError> {
    let opt = req.provider.clone().unwrap_or_default();

    let provider = find_provider(&opt, &pool)
        .await
        .context("Find provider failed")?;

    if !provider.enabled {
        return Err(ShortenError::ProviderIsNotEnabled(opt));
    }

    let response = match opt {
        ProviderOpt::Tinyurl => shorten_tinyurl(&req.url, &tinyurl).await,
        ProviderOpt::Bitly => shorten_bitly(&req.url, &bitly).await,
    }?;

    inc_url_conversions(&provider, &pool)
        .await
        .context("Failed to increase url conversions.")?;

    Ok(response)
}
