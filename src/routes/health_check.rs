use actix_web::{get, HttpResponse, Responder};

#[utoipa::path(
    responses(
        (status=200, description="Service is healthy.")
    )
)]
#[get("/api/health-check")]
#[tracing::instrument()]
pub async fn health_check() -> impl Responder {
    HttpResponse::Ok().await
}
