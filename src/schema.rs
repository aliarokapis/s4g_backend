// @generated automatically by Diesel CLI.

diesel::table! {
    providers (id) {
        id -> Int4,
        name -> Varchar,
        url_conversions -> Int4,
        enabled -> Bool,
    }
}
