use serde::{Deserialize, Serialize};
use utoipa::ToSchema;

#[derive(Serialize, Deserialize, Debug, ToSchema, Clone)]
#[schema(value_type = String)]
pub struct Url(url::Url);

#[derive(Serialize, Deserialize, Debug, ToSchema, Clone)]
#[schema(value_type = String)]
pub struct Host(url::Host);
