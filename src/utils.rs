use deadpool::managed::PoolError;
use deadpool_diesel::postgres::{Manager, Pool};

#[derive(thiserror::Error, Debug)]
pub enum QueryError {
    #[error("Failed to retrieve connection from Pool")]
    DbPoolError(#[from] PoolError<<Manager as deadpool::managed::Manager>::Error>),
    #[error("Query failed.")]
    DbQueryError(#[from] diesel::result::Error),
}

#[tracing::instrument(skip(pool, func))]
pub async fn query<F, T>(pool: &Pool, func: F) -> Result<T, QueryError>
where
    T: Sized + Send + 'static,
    F: FnOnce(&mut diesel::pg::PgConnection) -> diesel::QueryResult<T> + Send + 'static,
{
    let conn = pool.get().await?;

    let current_span = tracing::Span::current();

    Ok(conn
        .interact(move |conn| {
            current_span.in_scope(move || {
                tracing::info_span!("Executing sql query").in_scope(|| func(conn))
            })
        })
        .await
        .unwrap()?)
}
