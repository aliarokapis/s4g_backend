use actix_web::rt;
use anyhow::Context;
use opentelemetry::global;
use opentelemetry::sdk;
use opentelemetry::sdk::export::metrics::aggregation::cumulative_temporality_selector;
use opentelemetry::sdk::metrics::selectors;
use opentelemetry_otlp::WithExportConfig;
use tracing_log::LogTracer;
use tracing_subscriber::filter::EnvFilter;
use tracing_subscriber::fmt::MakeWriter;
use tracing_subscriber::layer::SubscriberExt;

pub fn get_subscriber<Sink>(
    name: String,
    env_filter: String,
    sink: Sink,
) -> impl SubscriberExt + Send + Sync + for<'span> tracing_subscriber::registry::LookupSpan<'span>
where
    Sink: for<'a> MakeWriter<'a> + Send + Sync + 'static,
{
    let env_filter =
        EnvFilter::try_from_default_env().unwrap_or_else(|_| EnvFilter::new(env_filter));

    let formatting_layer = tracing_bunyan_formatter::BunyanFormattingLayer::new(name, sink);

    let storage_layer = tracing_bunyan_formatter::JsonStorageLayer;

    tracing_subscriber::Registry::default()
        .with(env_filter)
        .with(storage_layer)
        .with(formatting_layer)
}

pub fn set_subscriber(subscriber: impl SubscriberExt + Send + Sync) -> anyhow::Result<()> {
    tracing::subscriber::set_global_default(subscriber)
        .context("Failed to set global subscriber.\n")?;

    LogTracer::init().context("Failed to set log tracer.")?;

    Ok(())
}

pub fn install_telemetry(
    subscriber: impl SubscriberExt
        + Send
        + Sync
        + for<'span> tracing_subscriber::registry::LookupSpan<'span>,
) -> anyhow::Result<()> {
    global::set_text_map_propagator(sdk::propagation::TraceContextPropagator::new());

    let tracer = opentelemetry_otlp::new_pipeline()
        .tracing()
        .with_exporter(opentelemetry_otlp::new_exporter().tonic().with_env())
        .install_batch(opentelemetry::runtime::Tokio)
        .context("Failed to install otlp tracer")?;

    let meter_provider = opentelemetry_otlp::new_pipeline()
        .metrics(
            selectors::simple::histogram(
                [
                    0.0, 2.5, 5.0, 7.5, 10.0, 12.5, 15.0, 17.5, 20.0, 22.5, 25.0, 30.0, 35.0, 40.0,
                    50.0, 60.0, 70.0, 80.0, 90.0, 100.0, 125.0, 250.0, 500.0, 750.0, 1000.0,
                    2500.0, 5000.0, 7500.0, 10000.0,
                ]
                .to_vec(),
            ),
            cumulative_temporality_selector(),
            opentelemetry::runtime::Tokio,
        )
        .with_exporter(opentelemetry_otlp::new_exporter().tonic().with_env())
        .build()
        .context("Failed to construct meter provider.")?;

    global::set_meter_provider(meter_provider);

    let telemetry = tracing_opentelemetry::layer().with_tracer(tracer);

    let subscriber = subscriber.with(telemetry);

    set_subscriber(subscriber)?;

    Ok(())
}

pub async fn terminate_telemetry() -> std::io::Result<()> {
    rt::task::spawn_blocking(global::shutdown_tracer_provider).await?;

    Ok(())
}
