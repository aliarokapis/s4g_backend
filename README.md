# S4G Url Shortener REST api service.

This project implements a REST api service for shortening URLs using the following providers:

- [Bitly](https://bitly.com/)
- [Tinyurl](https://tinyurl.com/)

# OpenAPI

The service provides endpoints for serving [OpenApi](https://swagger.io/specification/) definitions:

- `/api/v1/user-api-doc/openapi.json`: The openapi definition for the user-facing functionality.
- `/api/v1/admin-api-doc/openapi.json`: The openapi definition for the administrative functionality.

We also embed a [SwaggerUI](https://swagger.io/tools/swagger-ui/) instance running at `/swagger-ui/` 
for easier API visualization.

# Observability

The service is compatible with the [opentelemetry](https://opentelemetry.io/) standard and 
supports distributed tracing and metrics.

# Configuration

We follow the [12 Factor App](https://12factor.net) guidelines for configuration.
The following environment variables are supported:

- `S4G__APP__HOST`: The server's bind address.
- `S4G__APP__PORT`: The server's port.
- `S4G__APP__ALLOWED_ORIGINS`: A list of urls for supporting CORS.
- `S4G__APP__THREAD_POOL_SIZE`: The size of the thread pool used for the database connections.
- `S4G__DATABASE__HOST`: The address of the PostgreSQL database.
- `S4G__DATABASE__USERNAME`: The database username.
- `S4G__DATABASE__PASSWORD`: The database password.
- `S4G__DATABASE__DB_NAME`: The name of the database.
- `S4G__BITLY__BASE_URL`: The base url for the bitly API [eg. https://api-ssl.bitly.com].
- `S4G__BITLY__BEARER`: The bitly bearer token.
- `S4G__TINYURL__BASE_URL`: The base url for the tinyurl API [eg. https://api.tinyurl.com].
- `S4G__TINYURL__BEARER`: The tinyurl bearer token.

We also support [opentelemetry standard variables](https://opentelemetry.io/docs/reference/specification/sdk-environment-variables/) such as:

- `OTEL_RESOURCE_ATTRIBUTES`: Used to configure resource attributes such as service name and namespace.
- `OTEL_EXPORTER_OTLP_ENDPOINT`: The OTLP-compatible endpoint where the batched data will be exported to.

The project also supports gitignored-`.env` files at the project root for easier development.

# Migrations

The project relies on the [Diesel](https://diesel.rs/) ORM. 

The easiest way to perform database migrations is through the 
[`Diesel CLI`](https://crates.io/crates/diesel_cli) which can be globally installed by running `cargo install diesel_cli`.

The migrations can be applied by running `diesel migrations (run|up|down)`. 

A database can be created and all the migrations applied
by running `diesel setup`. 

The Diesel CLI requires that a the database url is available either through the `DATABASE_URL` environment variable,
or through the `--database-url` cli option. 

We suggest looking at the official Diesel docs for more info.

The service does not setup any database or run any migrations itself due to clustering considerations.

# Building the service

The service can be built by running `cargo build` at the project root.

This generates an executable under the `target` folder.

# Running the service

The service can be run directly through the executable located in the post-build `target` directly or through `cargo run`.

All environment variables must be present and the provided database must be initialized with all the migrations pre-applied.

An easier approach for deployment is to use our provided [Docker images](#docker).

# Testing the service

The `tests` folder contains integration tests that require a running PostgresSQL instance.

By default we provide a minimal `docker-compose.yml` file that provides a compatible instance.

The `.env.tests.default` file provide the docker-compatible credentials for the test suite which can be overridden 
through environment variables or through a gitignored-`.env.tests` file.

Each test automatically creates a separate database and runs all migrations directly so Diesel CLI usage is not required.

The easiest way to execute the tests is by running:

- `docker compose up` at the project root.
- `cargo test`

# Docker

In order to support container orchestration workflows and platform-agnostic deployments, we provide images for both the main service
and also for the migration utilities.

A concrete example of using the Docker images can be found at the [demo repository](https://gitlab.com/s4g_url_shortener/s4g_demo).

# Examples

## Health check

### Request

```bash
curl -i -X GET '${SERVICE_URL}/api/health-check'
```
### Response

```bash
HTTP/1.1 200 OK
```

## Shortening links

### Request
```bash
curl -i \
-X PUT '${SERVICE_URL}/api/v1/shorten' \
-H 'Content-Type: application/json' \
-d '{ "url": "http://very-long-url.com", "provider": "tinyurl"}'
```
### Response

```bash
HTTP/1.1 200 OK
content-type: application/json

{"short_url" : "https://tinyurl.com/abcdef" } 
```

## (Admin) Retrieving All Provider Info

### Request

```bash
curl -i -X GET '${SERVICE_URL}/api/v1/provider'
```

### Response
```bash
HTTP/1.1 200 OK
content-type: application/json

[
    {"provider": "bitly", "url_conversions": 123, "enabled": false},
    {"provider": "tinyurl", "url_conversions": 456, "enabled": true}
]
```

## (Admin) Retrieving Specific Provider Info

### Request

```bash
curl -i -X GET '${SERVICE_URL}/api/v1/provider/bitly'
```

### Response
```bash
HTTP/1.1 200 OK
content-type: application/json

{"provider": "bitly", "url_conversions": 123, "enabled": false}
```

## (Admin) Set Specific Provider Status

### Request

```bash
curl -i \
-X PUT '${SERVICE_URL}/api/v1/provider/tinyurl/status' \
-H 'Content-Type: application/json' \
-d '{ "enable": false }'
```

### Response
```bash
HTTP/1.1 200 OK
```
