use diesel::connection::{Connection, SimpleConnection};
use diesel::pg::PgConnection;
use diesel_migrations::{embed_migrations, EmbeddedMigrations, MigrationHarness};
use once_cell::sync::Lazy;
use s4g_backend::config::{Config, DatabaseConfig};
use s4g_backend::startup::Application;
use s4g_backend::telemetry::{get_subscriber, set_subscriber};
use wiremock::MockServer;

static TRACING: Lazy<()> = Lazy::new(|| {
    dotenvy::from_filename(".env.tests").ok();
    dotenvy::from_filename(".env.tests.default").unwrap();

    let default_filter_level = "info".to_string();
    let subscriber_name = "test".to_string();
    if std::env::var("TEST_LOG").is_ok() {
        let subscriber = get_subscriber(subscriber_name, default_filter_level, std::io::stdout);
        set_subscriber(subscriber).unwrap();
    } else {
        let subscriber = get_subscriber(subscriber_name, default_filter_level, std::io::sink);
        set_subscriber(subscriber).unwrap();
    };
});

pub struct TestApp {
    pub address: String,
    pub api_client: reqwest::Client,
    pub mock_server: MockServer,
    db_config: DatabaseConfig,
}

impl Drop for TestApp {
    fn drop(&mut self) {
        destroy_database(&self.db_config);
    }
}

impl TestApp {
    pub async fn new() -> TestApp {
        Lazy::force(&TRACING);

        let mock_server = MockServer::start().await;

        let config = Config::from_env()
            .set_app_host("localhost".into())
            .set_app_port(0)
            .set_app_thread_pool_size(1)
            .set_app_allowed_origins(vec![])
            .set_database_db_name(uuid::Uuid::new_v4().to_string())
            .set_bitly_base_url(mock_server.uri())
            .set_bitly_bearer("<secret-unused>".into())
            .set_tinyurl_base_url(mock_server.uri())
            .set_tinyurl_bearer("<secret-unused>".into())
            .build()
            .expect("Failed to read configuration.");

        setup_database(&config.database);

        let app = Application::build(&config).await.unwrap();
        let app_port = app.port();

        let _ = actix_web::rt::spawn(app.run());

        let client = reqwest::Client::builder()
            .redirect(reqwest::redirect::Policy::none())
            .build()
            .unwrap();

        let test_app = TestApp {
            address: format!("http://localhost:{}", app_port),
            api_client: client,
            mock_server,
            db_config: config.database.clone(),
        };

        test_app
    }

    pub async fn get_providers(&self) -> reqwest::Response {
        self.api_client
            .get(&format!("{}/api/v1/provider", &self.address))
            .send()
            .await
            .expect("Failed to execute request")
    }

    pub async fn get_providers_json(&self) -> serde_json::Value {
        self.get_providers()
            .await
            .json::<serde_json::Value>()
            .await
            .expect("Failed to deserialize response")
    }

    pub async fn get_provider(&self, provider: &str) -> reqwest::Response {
        self.api_client
            .get(&format!("{}/api/v1/provider/{}", &self.address, provider))
            .send()
            .await
            .expect("Failed to execute request")
    }

    pub async fn get_provider_json(&self, provider: &str) -> serde_json::Value {
        self.get_provider(provider)
            .await
            .json::<serde_json::Value>()
            .await
            .expect("Failed to deserialize response")
    }

    pub async fn put_provider_status(
        &self,
        provider: &str,
        body: &impl serde::Serialize,
    ) -> reqwest::Response {
        self.api_client
            .put(&format!(
                "{}/api/v1/provider/{}/status",
                &self.address, provider
            ))
            .json(&body)
            .send()
            .await
            .expect("Failed to execute request")
    }

    pub async fn put_shorten(&self, body: &impl serde::Serialize) -> reqwest::Response {
        self.api_client
            .put(&format!("{}/api/v1/shorten", &self.address))
            .json(&body)
            .send()
            .await
            .expect("Failed to execute request")
    }

    pub async fn put_shorten_json(&self, body: &impl serde::Serialize) -> serde_json::Value {
        self.put_shorten(body)
            .await
            .json::<serde_json::Value>()
            .await
            .expect("Failed to deserialize response")
    }

    pub async fn mock_server(
        &self,
        path: &str,
        method: &str,
        status: u16,
        body: &impl serde::Serialize,
    ) -> wiremock::MockGuard {
        use wiremock::matchers::{method as method_rule, path as path_rule};
        use wiremock::{Mock, ResponseTemplate};

        Mock::given(path_rule(path))
            .and(method_rule(method))
            .respond_with(ResponseTemplate::new(status).set_body_json(body))
            .mount_as_scoped(&self.mock_server)
            .await
    }

    pub async fn mock_server_bitly(
        &self,
        status: u16,
        body: &impl serde::Serialize,
    ) -> wiremock::MockGuard {
        self.mock_server("/v4/shorten", "POST", status, body).await
    }

    pub async fn mock_server_tinyurl(
        &self,
        status: u16,
        body: &impl serde::Serialize,
    ) -> wiremock::MockGuard {
        self.mock_server("/create", "POST", status, body).await
    }
}

const MIGRATIONS: EmbeddedMigrations = embed_migrations!();

fn setup_database(config: &DatabaseConfig) {
    let mut connection =
        PgConnection::establish(&config.without_db_name()).expect("Failed to connect to Postgres");

    connection
        .batch_execute(&format!(r#"CREATE DATABASE "{}";"#, config.db_name))
        .expect("Failed to construct scratch database");

    let mut connection =
        PgConnection::establish(&config.with_db_name()).expect("Failed to connect to Postgres");

    connection
        .run_pending_migrations(MIGRATIONS)
        .expect("Failed to run migrations");
}

fn destroy_database(config: &DatabaseConfig) {
    let mut connection =
        PgConnection::establish(&config.without_db_name()).expect("Failed to connect to Postgres");

    connection
        .batch_execute(&format!(
            r#"DROP DATABASE "{}" WITH (FORCE);"#,
            config.db_name
        ))
        .expect("Failed to delete scratch database");
}
