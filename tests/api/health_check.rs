use crate::utils::TestApp;

#[actix_web::rt::test]
async fn health_check_works() {
    let app = TestApp::new().await;

    let response = app
        .api_client
        .get(&format!("{}/api/health-check", &app.address))
        .send()
        .await
        .expect("Failed to execute request.");

    assert!(response.status().is_success());
    assert_eq!(Some(0), response.content_length());
}
