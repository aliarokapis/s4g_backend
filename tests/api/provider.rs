use reqwest::StatusCode;
use serde_json::json;

use crate::utils::TestApp;

fn default_provider(provider: &str) -> serde_json::Value {
    json!({
        "provider" : provider,
        "url_conversions" : 0,
        "enabled" : true
    })
}

#[actix_web::rt::test]
async fn index_returns_200() {
    let app = TestApp::new().await;

    let response = app.get_providers().await;

    assert!(response.status().is_success());
}

#[actix_web::rt::test]
async fn index_initially_returns_default_providers() {
    let app = TestApp::new().await;

    let response = app.get_providers_json().await;

    assert_eq!(
        response,
        json!([default_provider("bitly"), default_provider("tinyurl")])
    );
}

#[actix_web::rt::test]
async fn show_valid_provider_returns_200() {
    let app = TestApp::new().await;

    let response = app.get_provider("bitly").await;

    assert!(response.status().is_success());
}

#[actix_web::rt::test]
async fn show_bitly_initially_returns_default() {
    let app = TestApp::new().await;

    let response = app.get_provider_json("bitly").await;

    assert_eq!(response, default_provider("bitly"))
}

#[actix_web::rt::test]
async fn show_tinyurl_returns_200() {
    let app = TestApp::new().await;

    let response = app.get_provider("tinyurl").await;

    assert!(response.status().is_success());
}

#[actix_web::rt::test]
async fn show_tinyurl_initially_returns_default() {
    let app = TestApp::new().await;

    let response = app.get_provider_json("tinyurl").await;

    assert_eq!(response, default_provider("tinyurl"));
}

#[actix_web::rt::test]
async fn show_other_provider_returns_not_found() {
    let app = TestApp::new().await;

    let response = app.get_provider("other").await;

    assert_eq!(response.status(), StatusCode::NOT_FOUND);
}

#[actix_web::rt::test]
async fn put_bitly_status_enable_true_returns_success() {
    let app = TestApp::new().await;

    let response = app
        .put_provider_status("bitly", &json!({ "enable" : true }))
        .await;

    assert!(response.status().is_success());
}

#[actix_web::rt::test]
async fn put_bitly_status_enable_false_returns_success() {
    let app = TestApp::new().await;

    let response = app
        .put_provider_status("bitly", &json!({ "enable" : false }))
        .await;

    assert!(response.status().is_success());
}

#[actix_web::rt::test]
async fn put_bitly_status_wrong_body_returns_bad_request() {
    let app = TestApp::new().await;

    let response = app
        .put_provider_status("bitly", &json!({ "wrong" : true }))
        .await;

    assert_eq!(response.status(), StatusCode::BAD_REQUEST);
}

#[actix_web::rt::test]
async fn put_tinyurl_status_enable_true_returns_success() {
    let app = TestApp::new().await;

    let response = app
        .put_provider_status("tinyurl", &json!({ "enable" : true }))
        .await;

    assert!(response.status().is_success());
}

#[actix_web::rt::test]
async fn put_tinyurl_status_enable_false_returns_success() {
    let app = TestApp::new().await;

    let response = app
        .put_provider_status("tinyurl", &json!({ "enable" : false }))
        .await;

    assert!(response.status().is_success());
}

#[actix_web::rt::test]
async fn put_other_provider_status_correct_body_returns_not_found() {
    let app = TestApp::new().await;

    let response = app
        .put_provider_status("other", &json!({ "enable" : true }))
        .await;

    assert_eq!(response.status(), StatusCode::NOT_FOUND);
}

#[actix_web::rt::test]
async fn put_other_provider_status_wrong_body_returns_not_found() {
    let app = TestApp::new().await;

    let response = app.put_provider_status("other", &json!({})).await;

    assert_eq!(response.status(), StatusCode::NOT_FOUND);
}

#[actix_web::rt::test]
async fn put_provider_status_to_false_then_show_returns_default_with_disabled() {
    let app = TestApp::new().await;

    let provider = "bitly";

    app.put_provider_status(provider, &json!({ "enable" : false }))
        .await;
    let response = app.get_provider_json(provider).await;

    assert_eq!(
        response,
        json!({
            "provider" : provider,
            "url_conversions" : 0,
            "enabled" : false
        })
    );
}

#[actix_web::rt::test]
async fn put_provider_status_to_false_then_true_then_show_returns_default() {
    let app = TestApp::new().await;

    let provider = "bitly";

    app.put_provider_status(provider, &json!({ "enable" : false }))
        .await;
    app.put_provider_status(provider, &json!({ "enable" : true }))
        .await;
    let response = app.get_provider_json(provider).await;

    assert_eq!(response, default_provider(provider));
}
