use reqwest::StatusCode;
use serde_json::json;

use crate::utils::TestApp;

const LONG_URL_EXAMPLE: &'static str = "https://long_url.com/";
const SHORTENED_URL_EXAMPLE: &'static str = "https://short_url.com/";
const VALID_PROVIDER_EXAMPLE: &'static str = "bitly";

fn successful_bitly_response() -> serde_json::Value {
    json!({ "link": SHORTENED_URL_EXAMPLE })
}

fn successful_tinyurl_response() -> serde_json::Value {
    json!({
        "code" : 0,
        "data" : {
            "tiny_url": SHORTENED_URL_EXAMPLE
        }
    })
}

#[actix_web::rt::test]
async fn correct_shorten_usage_with_bitly_is_successful_if_bitly_is_enabled_and_responds_properly()
{
    let app = TestApp::new().await;

    let _mock_guard = app
        .mock_server_bitly(200, &successful_bitly_response())
        .await;

    let response = app
        .put_shorten(&json!({
            "provider" : "bitly",
            "url": LONG_URL_EXAMPLE
        }))
        .await;

    assert!(response.status().is_success());
}

#[actix_web::rt::test]
async fn correct_shorten_usage_with_bitly_returns_shortened_url_if_bitly_is_enabled_and_responds_properly(
) {
    let app = TestApp::new().await;

    let _mock_guard = app
        .mock_server_bitly(200, &successful_bitly_response())
        .await;

    let response = app
        .put_shorten_json(&json!({
            "provider" : "bitly",
            "url": LONG_URL_EXAMPLE
        }))
        .await;

    assert_eq!(response, json!({ "short_url": SHORTENED_URL_EXAMPLE }));
}

#[actix_web::rt::test]
async fn missing_provider_in_shorten_request_acts_the_same_as_bitly() {
    let app = TestApp::new().await;

    let _mock_guard = app
        .mock_server_bitly(200, &json!({ "link": SHORTENED_URL_EXAMPLE }))
        .await;

    let response = app
        .put_shorten_json(&json!({ "url": LONG_URL_EXAMPLE }))
        .await;

    assert_eq!(response, json!({ "short_url": SHORTENED_URL_EXAMPLE }));
}

#[actix_web::rt::test]
async fn correct_shorten_usage_with_tinyurl_is_successful_if_tinyurl_is_enabled_and_responds_properly(
) {
    let app = TestApp::new().await;

    let _mock_guard = app
        .mock_server_tinyurl(200, &successful_tinyurl_response())
        .await;

    let response = app
        .put_shorten(&json!({
            "provider" : "tinyurl",
            "url": LONG_URL_EXAMPLE
        }))
        .await;

    assert!(response.status().is_success());
}

#[actix_web::rt::test]
async fn correct_shorten_usage_with_tinyurl_returns_shortened_url_if_tinyurl_is_enabled_and_responds_properly(
) {
    let app = TestApp::new().await;

    let _mock_guard = app
        .mock_server_tinyurl(200, &successful_tinyurl_response())
        .await;

    let response = app
        .put_shorten_json(&json!({
            "provider" : "tinyurl",
            "url": LONG_URL_EXAMPLE
        }))
        .await;

    assert_eq!(response, json!({ "short_url": SHORTENED_URL_EXAMPLE }));
}

#[actix_web::rt::test]
async fn using_invalid_provider_in_shorten_request_returns_bad_request() {
    let app = TestApp::new().await;

    let response = app
        .put_shorten(&json!({
            "provider" : "other",
            "url": LONG_URL_EXAMPLE
        }))
        .await;

    assert_eq!(response.status(), StatusCode::BAD_REQUEST);

    let text = response.text().await.unwrap();
    assert!(text.contains("unknown variant"));
}

#[actix_web::rt::test]
async fn missing_url_in_shorten_request_returns_bad_request() {
    let app = TestApp::new().await;

    let response = app
        .put_shorten(&json!({
            "provider" : VALID_PROVIDER_EXAMPLE,
            "not_url": LONG_URL_EXAMPLE
        }))
        .await;

    assert_eq!(response.status(), StatusCode::BAD_REQUEST);

    let text = response.text().await.unwrap();
    assert!(text.contains("missing field"));
}

#[actix_web::rt::test]
async fn using_invalid_url_in_shorten_request_returns_bad_request() {
    let app = TestApp::new().await;

    let response = app
        .put_shorten(&json!({
            "provider" : VALID_PROVIDER_EXAMPLE,
            "url": "invalid-url"
        }))
        .await;

    assert_eq!(response.status(), StatusCode::BAD_REQUEST);

    let text = response.text().await.unwrap();
    assert!(text.contains("invalid value"));
}

#[actix_web::rt::test]
async fn disabling_bitly_then_using_shorten_returns_conflict() {
    let app = TestApp::new().await;

    app.put_provider_status("bitly", &json!({ "enable" : false }))
        .await;

    let response = app
        .put_shorten(&json!({
            "provider" : "bitly",
            "url": LONG_URL_EXAMPLE
        }))
        .await;

    assert_eq!(response.status(), StatusCode::CONFLICT);

    let text = response.text().await.unwrap();
    assert!(text.contains("Provider bitly is currently disabled."));
}

#[actix_web::rt::test]
async fn disabling_tinyurl_then_using_shorten_returns_conflict() {
    let app = TestApp::new().await;

    app.put_provider_status("tinyurl", &json!({ "enable" : false }))
        .await;

    let response = app
        .put_shorten(&json!({
            "provider" : "tinyurl",
            "url": LONG_URL_EXAMPLE
        }))
        .await;

    assert_eq!(response.status(), StatusCode::CONFLICT);

    let text = response.text().await.unwrap();
    assert!(text.contains("Provider tinyurl is currently disabled."));
}

#[actix_web::rt::test]
async fn disabling_then_enabling_bitly_then_using_shorten_responds_successfully() {
    let app = TestApp::new().await;

    app.put_provider_status("bitly", &json!({ "enable" : false }))
        .await;

    let _mock_guard = app
        .mock_server_bitly(200, &successful_bitly_response())
        .await;

    app.put_provider_status("bitly", &json!({ "enable" : true }))
        .await;

    let response = app
        .put_shorten(&json!({
            "provider" : "bitly",
            "url": LONG_URL_EXAMPLE
        }))
        .await;

    assert!(response.status().is_success());
}

#[actix_web::rt::test]
async fn disabling_then_enabling_tinyurl_then_using_shorten_responds_successfully() {
    let app = TestApp::new().await;

    app.put_provider_status("tinyurl", &json!({ "enable" : false }))
        .await;

    let _mock_guard = app
        .mock_server_tinyurl(200, &successful_tinyurl_response())
        .await;

    app.put_provider_status("tinyurl", &json!({ "enable" : true }))
        .await;

    let response = app
        .put_shorten(&json!({
            "provider" : "tinyurl",
            "url": LONG_URL_EXAMPLE
        }))
        .await;

    assert!(response.status().is_success());
}

#[actix_web::rt::test]
async fn successfully_shortening_a_url_with_bitly_then_fetching_using_show_propertly_reports_conversion(
) {
    let app = TestApp::new().await;

    let _mock_guard = app
        .mock_server_bitly(200, &successful_bitly_response())
        .await;

    let make_request = || async {
        app.put_shorten(&json!({
            "provider" : "bitly",
            "url": LONG_URL_EXAMPLE
        }))
        .await;
    };

    make_request().await;
    make_request().await;

    let response = app.get_provider_json("bitly").await;

    assert_eq!(
        response,
        json!({
           "provider" : "bitly",
            "url_conversions" : 2,
            "enabled" : true,
        })
    );
}

#[actix_web::rt::test]
async fn successfully_shortening_a_url_with_tinyurl_then_fetching_using_show_propertly_reports_conversions(
) {
    let app = TestApp::new().await;

    let _mock_guard = app
        .mock_server_tinyurl(200, &successful_tinyurl_response())
        .await;

    let make_request = || async {
        app.put_shorten(&json!({
            "provider" : "tinyurl",
            "url": LONG_URL_EXAMPLE
        }))
        .await;
    };

    make_request().await;
    make_request().await;

    let response = app.get_provider_json("tinyurl").await;

    assert_eq!(
        response,
        json!({
           "provider" : "tinyurl",
            "url_conversions" : 2,
            "enabled" : true,
        })
    );
}

#[actix_web::rt::test]
async fn disabling_bitly_then_calling_shorten_properly_does_not_increase_conversions() {
    let app = TestApp::new().await;

    let make_request = || async {
        app.put_shorten(&json!({
            "provider" : "bitly",
            "url": LONG_URL_EXAMPLE
        }))
        .await;
    };

    app.put_provider_status("bitly", &json!({ "enable" : false }))
        .await;

    make_request().await;
    make_request().await;

    let response = app.get_provider_json("bitly").await;

    assert_eq!(
        response,
        json!({
           "provider" : "bitly",
            "url_conversions" : 0,
            "enabled" : false,
        })
    );
}

#[actix_web::rt::test]
async fn disabling_tinyurl_then_calling_shorten_properly_does_not_increase_conversions() {
    let app = TestApp::new().await;

    let make_request = || async {
        app.put_shorten(&json!({
            "provider" : "tinyurl",
            "url": LONG_URL_EXAMPLE
        }))
        .await;
    };

    app.put_provider_status("tinyurl", &json!({ "enable" : false }))
        .await;

    make_request().await;
    make_request().await;

    let response = app.get_provider_json("tinyurl").await;

    assert_eq!(
        response,
        json!({
           "provider" : "tinyurl",
            "url_conversions" : 0,
            "enabled" : false,
        })
    );
}

#[actix_web::rt::test]
async fn failing_to_convert_using_bitly_does_not_increase_conversions() {
    let app = TestApp::new().await;

    let make_request = || async {
        app.put_shorten(&json!({
            "provider" : "bitly",
            "url": "bad-url"
        }))
        .await;
    };

    make_request().await;
    make_request().await;

    let response = app.get_provider_json("bitly").await;

    assert_eq!(
        response,
        json!({
           "provider" : "bitly",
            "url_conversions" : 0,
            "enabled" : true,
        })
    );
}

#[actix_web::rt::test]
async fn failing_to_convert_using_tinyurl_does_not_increase_conversions() {
    let app = TestApp::new().await;

    let make_request = || async {
        app.put_shorten(&json!({
            "provider" : "tinyurl",
            "url": "bad-url"
        }))
        .await;
    };

    make_request().await;
    make_request().await;

    let response = app.get_provider_json("tinyurl").await;

    assert_eq!(
        response,
        json!({
           "provider" : "tinyurl",
            "url_conversions" : 0,
            "enabled" : true,
        })
    );
}

#[actix_web::rt::test]
async fn correct_synergy_between_put_status_shortenings_and_index() {
    let app = TestApp::new().await;

    let make_request = |provider: &'static str| {
        let app_ref: &TestApp = &app;
        async move {
            app_ref
                .put_shorten(&json!({
                    "provider" : provider,
                    "url": LONG_URL_EXAMPLE
                }))
                .await
        }
    };

    let _bitly_guard = app
        .mock_server_bitly(200, &successful_bitly_response())
        .await;
    let _tinyurl_guard = app
        .mock_server_tinyurl(200, &successful_tinyurl_response())
        .await;

    make_request("bitly").await;
    make_request("bitly").await;
    app.put_provider_status("bitly", &json!({ "enable" : false }))
        .await;
    make_request("tinyurl").await;
    make_request("tinyurl").await;
    make_request("tinyurl").await;
    make_request("tinyurl").await;
    app.put_provider_status("bitly", &json!({ "enable" : true }))
        .await;
    make_request("bitly").await;
    app.put_provider_status("tinyurl", &json!({ "enable" : false }))
        .await;
    make_request("tinyurl").await;

    let response = app.get_providers_json().await;

    assert_eq!(
        response,
        json!([
            {
                "provider" : "bitly",
                "url_conversions" : 3,
                "enabled" : true,
            },
            {
                "provider" : "tinyurl",
                "url_conversions" : 4,
                "enabled" : false,
            }
        ])
    );
}
